# Bash
export CLICOLOR=1
alias ll="ls -al"
alias h="history"
alias tasks="mvim ~/Dropbox/tasks.markdown"
# Don't clobber bash history
shopt -s histappend

# Line endings
mac2unix() {
    for file
    do
        echo "Converting $file"
        perl -pi -e 's/\r/\n/g' "$file"
    done
}

# Prompt
export VIRTUAL_ENV_DISABLE_PROMPT=1
parse_virtualenv() {
    if [ -n "$VIRTUAL_ENV" ] ; then
        basename $VIRTUAL_ENV | sed -e 's/\(.*\)/(\1) /'
    fi
}
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1) /'
}
PS1="\n\$(parse_virtualenv)\$(parse_git_branch)\h \e[0;31m\w\e[m\n\$ "

# Python
source /usr/local/bin/virtualenvwrapper.sh

# Pip
_pip_completion()
{
    COMPREPLY=( $( COMP_WORDS="${COMP_WORDS[*]}" \
                   COMP_CWORD=$COMP_CWORD \
                   PIP_AUTO_COMPLETE=1 $1 ) )
}
complete -o default -F _pip_completion pip


# Git
source ~/.git-completion.sh
alias gs="git status"
alias gd="git diff"
alias gl="git log"
alias glp="git log -p"
alias gb="git branch"
alias gp="git pull"

# Mongo
export PATH=$PATH:/usr/local/mongodb/bin

# MySQL
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/usr/local/mysql/lib
export PATH=$PATH:/usr/local/mysql/bin

# Ack
export ACK_COLOR_MATCH="red"
export ACK_OPTIONS="-a -i"

# AWS
export JAVA_HOME="/System/Library/Frameworks/JavaVM.framework/Home"
export EC2_HOME="/usr/local/ec2-api-tools"
export PATH=$PATH:$EC2_HOME/bin
export EC2_PRIVATE_KEY=~/.aws/pk.pem
export EC2_CERT=~/.aws/cert.pem
export EC2_URL="https://ec2.eu-west-1.amazonaws.com"

# RabbitMQ
export PATH=$PATH:/usr/local/sbin

# Clojure
alias clj="java -cp /usr/local/clojure-1.4.0/clojure-1.4.0.jar clojure.main"
