if has("gui_macvim")
    macmenu &File.New\ Tab key=<nop>
    macmenu &File.Open\ Tab\.\.\. key=<nop>
    nnoremap <D-M-Left> <C-W>h
    nnoremap <D-M-Right> <C-W>l
    set guioptions-=r
endif
