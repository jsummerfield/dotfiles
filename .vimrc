" Pathogen
call pathogen#infect()
filetype plugin indent on

set modelines=0

set nocompatible
set number

" Allow mouse scroll
set mouse=a

" indicate a fast terminal - more redraw chars sent for smoother redraw
set ttyfast

" sane text files
set fileformat=unix
set fileformats=unix,dos,mac
set encoding=utf-8

" Save when losing focus
au FocusLost * silent! wa

" Tabs
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
set list
set listchars=extends:❯,precedes:❮,trail:·

" Use the system clipboard by default
set clipboard=unnamed

" Leader
let mapleader = ","

" shift plus movement keys changes selection
set keymodel=startsel,stopsel

" allow backgrounding buffers without writing them
" and remember marks/undo for backgrounded buffers
set hidden

" Make searches case-sensitive only if they contain upper-case characters
set ignorecase
set smartcase
" show search matches as the search pattern is typed
set incsearch

" don't flick the cursor to show matching brackets, they are already highlighted
set noshowmatch

" highlight last search matches
set hlsearch
" ctl-l doesn't just refresh window, but also hides search highlights
nnoremap <silent> <c-l> :nohlsearch<cr><c-l>
inoremap <silent> <c-l> <esc>:nohlsearch<cr><c-l>i

" search-next wraps back to start of file
set wrapscan

" make tab completion for files/buffers act like bash
set wildmenu
set wildmode=list:longest
" stuff to ignore when autocompleting filenames
set wildignore=*.pyc,*.pyo

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" Backups
set undodir=~/.vim/tmp/undo//     " undo files
set backupdir=~/.vim/tmp/backup// " backups
set directory=~/.vim/tmp/swap//   " swap files
set nobackup
set nowritebackup
set noswapfile
"set undofile

" Status bar
set laststatus=2

" Colour Scheme
syntax on
set background=dark
colorscheme badwolf
let g:badwolf_html_link_underline = 0

if has('gui_running')
    set guifont=Monaco:h12
    set go-=T
endif

if has("autocmd")
    " autocmd FileType python set ofu=syntaxcomplete#Complete

    " Markdown
    autocmd FileType markdown set commentstring=<!--\ %s\ -->

    " JSON
    au! BufRead,BufNewFile *.json set filetype=json

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    autocmd BufReadPost * call SetLastCursorPosition()
    function! SetLastCursorPosition()
        if line("'\"") > 0 && line("'\"") <= line("$")
            exe "normal g`\""
            normal! zz
        endif
    endfunction
endif " has("autocmd")

" make Y yank to end of line (consistent with C and D)
noremap Y y$

" make Q do somethign useful - format para
noremap Q gq}

" go to next/prev buffer
noremap <silent> <c-tab> :hide bp<cr>
map! <silent> <c-tab> <esc><c-tab>
noremap <silent> <s-c-tab> :hide bn<cr>
map! <silent> <s-c-tab> <esc><s-c-tab>

" Splitting a window will put the new window on the right
set splitright

" make cursor keys work during visual block selection
vnoremap <left> h
vnoremap <right> l
vnoremap <up> k
vnoremap <down> j

" Better Completion
set completeopt=menuone,preview

" Edit .vimrc
nnoremap <leader>ev <C-w><C-v><C-l>:e $MYVIMRC<cr>
" When vimrc is edited, reload it
autocmd! bufwritepost .vimrc source ~/.vimrc

" reselect pasted text
nnoremap <leader>r V`]

" Tags
" Keep working up the tree until the tags are found
set tags=tags;/
noremap <leader>T :!ctags -R --languages=python .<cr>

" jump to definition if only one match, otherwise show list
map <C-]> :exec("tjump ".expand("<cword>"))<CR>

" Tagbar
let g:tagbar_left = 1
" let g:tagbar_singleclick = 1
let g:tagbar_width = 50

map <leader>d :NERDTreeToggle<CR>
map <leader>f :NERDTreeFind<CR>
map <leader>t :TagbarToggle<CR>

" Search/Replace word under cursor
nnoremap <leader>S :%s/\<<C-r><C-w>\>//g<Left><Left>

" Ack
nnoremap <leader>a :Ack 

" Splits
nnoremap <leader>v :vnew<CR>

" Removes trailing spaces
function! TrimWhiteSpace()
    %s/\s*$//
    ''
:endfunction
noremap <leader>w :call TrimWhiteSpace()<CR>

" Supertab
" let g:SuperTabDefaultCompletionType = "<C-X><C-O>"

nnoremap j gj
nnoremap k gk

" Re-indent text after pasting
" nnoremap p ]p
" nnoremap P ]P

" Highlight lines > 80 chars
" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
" match OverLength /\%81v.\+/

" Syntastic
let g:syntastic_check_on_open = 1
let g:syntastic_python_checker = 'flake8'
let g:syntastic_python_checker_args='--ignore=E501,W391'
" let g:syntastic_auto_loc_list = 1

nnoremap <D-e> :lnext<CR>
nnoremap <D-E> :lfirst<CR>

" Colorscheme
map <F3> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">" . " FG:" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"fg#")<CR>

" Zen Coding
let g:user_zen_settings = {
\  'html' : {
\    'indentation' : '    '
\  },
\}

" Powerline
let g:Powerline_symbols = 'fancy'

set scrolljump=10
