# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="random"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git python fabric pip)

source $ZSH/oh-my-zsh.sh

# Prompt
parse_virtualenv() {
    if [ -n "$VIRTUAL_ENV" ] ; then
        basename $VIRTUAL_ENV | sed -e 's/\(.*\)/\1 /'
    fi
}

DISABLE_AUTO_TITLE=true

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[grey]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}*"
ZSH_THEME_GIT_PROMPT_CLEAN=""

PROMPT='
%{$fg_bold[grey]%}$(parse_virtualenv)%{$reset_color%}$(git_prompt_info)%{$fg[red]%}%~%{$reset_color%}
$ '

# Bash
alias ll="ls -al"
alias h="history"
alias todo="mvim ~/Dropbox/todo/todo.txt"

# Line endings
mac2unix() {
    for file
    do
        echo "Converting $file"
        perl -pi -e 's/\r/\n/g' "$file"
    done
}

# Python
export VIRTUAL_ENV_DISABLE_PROMPT=1
source /usr/local/bin/virtualenvwrapper.sh

# Git
source ~/.git-completion.sh
alias gs="git status"
alias gd="git diff"
alias gl="git log"
alias glp="git log -p"
alias gb="git branch"
alias gp="git pull"

# tmux
alias ta="tmux attach"

# Mongo
export PATH=$PATH:/usr/local/mongodb/bin

# MySQL
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/usr/local/mysql/lib
export PATH=$PATH:/usr/local/mysql/bin

# Ack
export ACK_COLOR_MATCH="red"
export ACK_OPTIONS="-a -i"

# AWS
export JAVA_HOME="/System/Library/Frameworks/JavaVM.framework/Home"
export EC2_HOME="/usr/local/ec2-api-tools"
export PATH=$PATH:$EC2_HOME/bin
export EC2_PRIVATE_KEY=~/.aws/pk.pem
export EC2_CERT=~/.aws/cert.pem
export EC2_URL="https://ec2.eu-west-1.amazonaws.com"

# RabbitMQ
export PATH=$PATH:/usr/local/sbin

# Clojure
alias clj="java -cp /usr/local/clojure-1.4.0/clojure-1.4.0.jar clojure.main"
